# Bulk CDX and CDX-derived cell line RNA-seq experiments 
# Sam Humphrey November 2020

#### These codes contain the analysis performed for the manuscripts below:
*Tumor plasticity ‘primes’ non-neuroendocrine SCLC cells for vascular mimicry (Manuscript in preparation)*

*S. M. Pearsall et al., “The rare YAP1 subtype of Small Cell Lung Cancer revisited in a biobank of 39 Circulating Tumour Cell Patient Derived eXplant models (CDX): A brief report,” J. Thorac. Oncol., 2020.*


## Data and availability
Two sets of RNA-seq data were analysed in both manuscripts, the bulk RNA-seq from CDX tumour tissue is accessible from *K. L. Simpson et al., “A biobank of small cell lung cancer CDX models elucidates inter- and intratumoral phenotypic heterogeneity,” Nat. Cancer, vol. 1, no. 4, pp. 437–451, 2020.*

Whilst the neuroendocrine and non-neuroendocrine cell line data is accessible from:
... 


## RNA-seq pipeline 

Both sets of data were processed the same way, using NGSCheckMate to QC for mislabelled samples [4]. Preliminary experiments in the bulk RNA-seq dataset identified the necessity of mouse filtering, therefore all samples were processed using mouse filtering. RNA-seq data was aligned to both *H. sapiens* and *M. musculus* genomes independently through the nf-core rna-seq pipeline, implemented in Nextflow [5, 6]. Sample quality control metrics from the pipeline were investigated before further processing. Aligned *.bam* files were filtered to removed any reads of mouse contamination using bamcmp [7], before assignment using FeatureCounts to the *H. sapiens* genome version 99 from Ensembl [8, 9]. Downstream analysis is documented within the *.Rmd* files.


### Nextflow details

```	
	nohup nextflow run nf-core/rnaseq --reads '/pathToFASTQ/*_R{1,2}*fastq.gz' --genome GRCh38_v99 -profile singularity -name humanRun -c nextflow.config

	nohup nextflow run nf-core/rnaseq --reads '/pathToFASTQ/*_R{1,2}*fastq.gz' --genome GRCm38_v99 -profile singularity -name mouseRun -c nextflow.config
```

### *runBAMcmp.sh*

The script *runBAMcmp.sh* requires two directories of *.bam* files: one aligned to human, one aligned to mouse, with the identical filenames. This can easily be created by moving the *.bam* files from the markduplicates folder in the results, to a new folder. 
The script sorts, run bamcmp, re-sort and merge the *.bam* files and output into a new directory called *BAMs_filtered*, both human and mouse *.bam* files have been kept individually.

### *featureCounts_Assignments.R*

A short R script to run FeatureCounts [8].


## Downstream analysis 

Analysis is detailed in *NEvsNNE_DownstreamAnalysis.Rmd* and *YAP1BriefReportAnalysis.Rmd*. It is worth noting that the results published in [2] contain the data for a new model (CDX31P) as well as a preliminary set of cell line data from 2 CDX models. An additional 2 model cell lines were added in [1], with batch effect correction applied between the separate runs in the *NEvsNNE_DownstreamAnalysis.Rmd* script.


## References 
[1] *Tumor plasticity ‘primes’ non-neuroendocrine SCLC cells for vascular mimicry (Manuscript in preparation)*

[2] *S. M. Pearsall et al., “The rare YAP1 subtype of Small Cell Lung Cancer revisited in a biobank of 39 Circulating Tumour Cell Patient Derived eXplant models (CDX): A brief report,” J. Thorac. Oncol., 2020.*

[3] *K. L. Simpson et al., “A biobank of small cell lung cancer CDX models elucidates inter- and intratumoral phenotypic heterogeneity,” Nat. Cancer, vol. 1, no. 4, pp. 437–451, 2020.*

[4] S. Lee, S. Lee, S. Ouellette, W. Y. Park, E. A. Lee, and P. J. Park, “NGSCheckMate: Software for validating sample identity in Next-generation sequencing studies within and across data types,” Nucleic Acids Res., vol. 45, no. 11, p. e103, 2017.

[5] P. A. Ewels et al., “The nf-core framework for community-curated bioinformatics pipelines,” Nature Biotechnology, vol. 38, no. 3. pp. 276–278, 2020.

[6] P. A. Ewels et al., “The nf-core framework for community-curated bioinformatics pipelines,” Nature Biotechnology, vol. 38, no. 3. pp. 276–278, 2020.

[7] G. Khandelwal et al., “Next-generation sequencing analysis and algorithms for PDX and CDX models,” Mol. Cancer Res., vol. 15, no. 8, pp. 1012–1016, 2017.

[8] Y. Liao, G. K. Smyth, and W. Shi, “FeatureCounts: An efficient general purpose program for assigning sequence reads to genomic features,” Bioinformatics, vol. 30, no. 7, pp. 923–930, 2014.

[9] F. Cunningham et al., “Ensembl 2019,” Nucleic Acids Res., vol. 47, no. D1, pp. D745–D751, 2019.
