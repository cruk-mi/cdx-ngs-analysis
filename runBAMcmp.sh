#!/bin/bash

#----------------------------------------
# Sam Humphrey, Febuary 2020
# Script to run bamcmp over a directory of bamfiles
# 
#----------------------------------------

dataDir="pathToData/"
outputDir="${dataDir}BAMs_filtered/"


workingDir="pathToWorkingDir/"
bamHumanDir="${workingDir}BAM_human/"
bamMouseDir="${workingDir}BAM_mouse/"


tmpDir="${workingDir}tmp/"

bamList="${workingDir}bamList.txt"

if [ -d "$outputDir" ]; then rm -r "$outputDir" ; fi
if [ -f "$bamList" ]; then rm "$bamList"; fi

mkdir $outputDir
mkdir -p $tmpDir
mkdir -p ${workingDir}outfiles/
printf '%s\n' $bamHumanDir* > $bamList

while IFS= read -r line || [ -n "$line" ]; do 
    f="${line%%.*}"
    filename=${f#"${f%/*}/"}
    echo "${filename} -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-"

	qsub <<-_EOF 
		#PBS -N runBAMcmp_${filename}
		#PBS -l nodes=1:ppn=16
		#PBS -l walltime=00:02:00:00 
		#PBS -j oe
		#PBS -o ${workingDir}outfiles/runBAMcmp_${filename}.out

		ml apps/bamcmp apps/samtools
		cd ${workingDir}

		samtools sort -@ 16 -n -o ${tmpDir}${filename}_human.sorted.bam ${bamHumanDir}${filename}.sortedByCoord.out.markDups.bam
		samtools sort -@ 16 -n -o ${tmpDir}${filename}_mouse.sorted.bam ${bamMouseDir}${filename}.sortedByCoord.out.markDups.bam

	    bamcmp  -s mapq \
		        -t 16 \
		        -1 ${tmpDir}${filename}_human.sorted.bam \
		        -2 ${tmpDir}${filename}_mouse.sorted.bam \
		        -a ${tmpDir}${filename}_human_only.bam \
		        -A ${tmpDir}${filename}_human_better.bam \
		        -b ${tmpDir}${filename}_mouse_only.bam \
		        -B ${tmpDir}${filename}_mouse_better.bam

		# Sort the sam files into genomic coordinates and convert to bams
		samtools sort -@ 16 -o ${tmpDir}${filename}_human_only.sorted.bam ${tmpDir}${filename}_human_only.bam
		samtools sort -@ 16 -o ${tmpDir}${filename}_human_better.sorted.bam ${tmpDir}${filename}_human_better.bam
		samtools sort -@ 16 -o ${tmpDir}${filename}_mouse_only.sorted.bam ${tmpDir}${filename}_mouse_only.bam
		samtools sort -@ 16 -o ${tmpDir}${filename}_mouse_better.sorted.bam ${tmpDir}${filename}_mouse_better.bam

		# merge the _only and _better files together
	    samtools merge -@ 16 ${outputDir}${filename}_human_allReads.bam ${tmpDir}${filename}_human_only.sorted.bam ${tmpDir}${filename}_human_better.sorted.bam
		samtools merge -@ 16 ${outputDir}${filename}_mouse_allReads.bam ${tmpDir}${filename}_mouse_only.sorted.bam ${tmpDir}${filename}_mouse_better.sorted.bam

		rm ${tmpDir}${filename}*.sorted.bam ${tmpDir}${filename}*.sam ${tmpDir}${filename}*only.bam ${tmpDir}${filename}*better.bam

	_EOF

done < "${bamList}"